import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as multer from 'multer'
import * as cors from 'cors'
import { config as envConfig } from 'dotenv'
import { root, getImages, getImage, uploadImage, deleteImage } from './src/controllers/'
import { imageFilter } from './src/utils/'

/* Setup */
envConfig()
const PORT = process.env.PORT || 3001
const upload = multer({
  dest: `${process.env.UPLOAD_PATH}/`,
  fileFilter: imageFilter
})

/* App */
const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())



// Routes
app.get('/', root)
app.post('/images', upload.single('file'), uploadImage)
app.get('/images', getImages)
app.get('/images/:id', getImage)
app.delete('/images/:id', deleteImage)

app.listen(PORT)
console.log(`Filely server is up at 0.0.0.0:${PORT}`)
