import { Image as ImageModel } from '../models'

/* Setup */
const Image = new ImageModel()

export default async (req, res) => {
  try{
    Image.all(images => res.status(200).send(images))
  }
  catch (err){
    res.sendStatus(400)
  }
}
