import { Image as ImageModel } from '../models'
import { deleteFile } from '../utils/'

/* Setup */
const Image = new ImageModel()

export default async (req, res) => {
  try{
    const { body: { id }, file } = req

    if(!file){
      res
        .status(400)
        .send({ error:{ message: "File is empty" } })
      return
    }

    Image.get(id, (image: any, err: any) => {
      if(err){
        res.status(500).send({ error: err })
        return
      } else if(!image){
        // New Upload
        Image.create(
          req.file,
          (image, err) => {
            if(err){
              res.status(500).send({ error: {
                message: 'Could not create file',
                err
              }})
              return
            }
            res.status(201).send(image)
          }
        )
        return
      }

      // Update Upload
      Image.update(
        id,
        req.file,
        (newImage, err) => {
          if(err){
            res.status(500).send({ error: {
              message: 'Could not create file',
              err
            }})
            return
          }

          deleteFile(image.path)
          res.status(201).send(newImage)
        }
      )
    })
  }
  catch (err){
    res.sendStatus(400)
  }
}
