import * as fs from 'fs'
import * as path from 'path'
import { Image as ImageModel } from '../models'

/* Setup */
const UPLOAD_PATH = process.env.UPLOAD_PATH
const Image = new ImageModel()

export default (req, res) => {
  try{
    if(!req.params.id){
      res.sendStatus(404)
      return
    }
    Image.get(req.params.id, (image, err) => {
      if(!image){
        res.sendStatus(404)
        return
      }

      res.setHeader('Content-Type', image.mimetype)
      fs.createReadStream(path.join(UPLOAD_PATH, image.filename)).pipe(res)
    })
  }
  catch (err){
    console.log("Error: ", err)
    res.sendStatus(400)
  }
}
