import { Image as ImageModel } from '../models'
import { deleteFile } from '../utils/'

/* Setup */
const Image = new ImageModel()

export default (req, res) => {
  try{
    const { id } = req.params
    if(!id){
      res.sendStatus(400)
      return
    }

    // Finding Image
    Image.get(id, (image, err) => {
      if(!image){
        res.sendStatus(404)
        return
      }

      // Deleting Image from database
      Image.delete(id, (err) => {
        if(err){
          res.status(501).send({ error: {
            message: 'Could not delete',
            err
          }})
          return
        }
        deleteFile(image.path) // Deleting file from upload directory
        res.status(200).send(true)
      })
    })
  }
  catch (err){
    console.log("Error: ", err)
    res.sendStatus(400)
  }
}
