import getImage from './getImage'
import getImages from './getImages'
import uploadImage from './uploadImage'
import deleteImage from './deleteImage'
import root from './root'


export {
  getImage,
  getImages,
  uploadImage,
  deleteImage,
  root
}
