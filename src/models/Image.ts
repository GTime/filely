import Database from './Database'
import { config as envConfig } from 'dotenv'

envConfig()
const DB = new Database(process.env.SQLITE_DB)
const DB_SCHEMA = `
  CREATE TABLE IF NOT EXISTS Images (
   id text NOT NULL UNIQUE PRIMARY KEY,
   fieldname text NOT NULL,
   originalname text NOT NULL ,
   encoding text NOT NULL ,
   mimetype text NOT NULL ,
   destination text NOT NULL ,
   filename text NOT NULL UNIQUE,
   path text NOT NULL ,
   size integer NOT NULL
  );
`

type Image = {
  fieldname: string,
  originalname: string,
  encoding: string,
  mimetype: string,
  destination: string,
  filename: string,
  path: string,
  size: number
}

export default class {
  constructor(){
    this.createTable()
  }
  private createTable(){
    // Creating TABLE
    DB.exec(DB_SCHEMA, (err) => err? console.log(err) : null)
  }
  public all(callback: any){
    const sql = ` SELECT * FROM Images `
    const params = []
    DB.all(
      sql,
      params,
      (err, users) => err? console.log(err) : callback(users)
    )
  }
  public get(id: string, callback: any){
    const sql = ` SELECT * FROM Images WHERE id = ?`
    const params = [id]

    DB.get(
      sql,
      params,
      (err, image) => err? callback(null, err) : callback(image, null)
    )
  }
  public create(Image: Image, callback: any){
    const sql = `
      INSERT INTO Images (
        id,
        fieldname,
        originalname,
        encoding,
        mimetype,
        destination,
        filename,
        path,
        size
      )
      VALUES(?,?,?,?,?,?,?,?,?);
    `
    const id = Image.filename
    const params = [
      id,
      Image.fieldname,
      Image.originalname,
      Image.encoding,
      Image.mimetype,
      Image.destination,
      Image.filename,
      Image.path,
      Image.size
    ]

    DB.run(sql, params, function(err){
      if(err){
        callback(null, err)
      }
      else{
        callback({ ...Image, id }, null)
      }
    })
  }
  public update(id: string, Image: Image, callback: any){
    try{
      const sql = `
        UPDATE Images
        SET
          fieldname = ?,
          originalname = ?,
          encoding = ?,
          mimetype = ?,
          destination = ?,
          filename = ?,
          path = ?,
          size = ?
        WHERE id = ?;
      `
      const params = [
        Image.fieldname,
        Image.originalname,
        Image.encoding,
        Image.mimetype,
        Image.destination,
        Image.filename,
        Image.path,
        Image.size,
        id,
      ]

      DB.run(
        sql,
        params,
        (err) => err? callback(null, err) : callback({ ...Image, id }, null)
      )
    } catch(err){
      console.log(err)
    }
  }
  public delete(id: string, callback: any){
    const sql = ` DELETE FROM Images WHERE id = ? `
    DB.run( sql, [ id ], (err, users) => callback(err))
  }
}
