import { verbose } from 'sqlite3'

const SQLite = verbose()

export default class extends SQLite.Database{
  path: string;

  constructor(path: string){
    super(path, (err) => {
      if(err){
        console.log(err)
        return
      }
      console.log(`Connected`)
    })
    this.path = path
  }
}
