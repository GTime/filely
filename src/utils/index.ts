import * as del from 'del'

// Image Filter
export const imageFilter = (req, file, cb) => {
  // accept image only
  if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)){
    return cb(new Error('Only image files are allowed!'), false)
  }
  if(file.size > 4026120){
    return cb(new Error('File is to large, max is 4mb!'), false)
  }
  cb(null, true)
}
export const deleteFile = (file) => {
  // Delete files
  // del.sync([`${file}`])
  del([`${file}`]).then(paths => paths)
}
export const cleanFolder = (folderPath) => {
  // Delete files inside folder but not the folder itself
  del.sync([`${folderPath}/**`, `!${folderPath}`])
}

/*-- Redis --*/
export const objectToRedisHash = (obj: {}) => {
  let hash = []

  for(var key in obj){
    if(obj.hasOwnProperty(key)){
      hash = [ ...hash, key, obj[key] ]
    }
  }
  return hash
}
