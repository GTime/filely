# filely

## Database configuration
To get this app working you need a database. The was built with a sqlite database. To get the app to work you need to include the database file.

1. Go to the _**build/database**_ folder (if the database folder is not found, create one)
2. Create a file called _**filely.db**_

Now your database has been configured

## End-Points

### Creating or uploading a new file
POST Method to:
https://filely.com/file_types

Example:
For all image files: *https://host_url/images*
